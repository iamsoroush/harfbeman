import logging
import os
import random
import string
import sys
from datetime import datetime

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, PicklePersistence, CallbackQueryHandler


def _get_logger(write_logs=True):
    root = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(formatter)
    root.addHandler(stream_handler)

    if write_logs:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        log_dir = os.path.join(dir_path, 'logs')
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        now = datetime.now()
        file_handler = logging.FileHandler(os.path.join(log_dir,
                                                        "bot_{}_{}-{}-{}.log".format(now.date(),
                                                                                     now.hour,
                                                                                     now.minute,
                                                                                     now.second)))
        file_handler.setFormatter(formatter)
        root.addHandler(file_handler)
    root.setLevel(logging.INFO)
    return root


telebot_logger = _get_logger(True)


class TelegramBot:

    def __init__(self, bot_token):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        persistent = PicklePersistence(filename=os.path.join(dir_path, 'bot_data.pkl'))
        self.updater = Updater(str(bot_token), persistence=persistent, use_context=True, workers=0)
        # self.updater = Updater(str(bot_token), use_context=True, workers=0)
        self.dispatcher = self.updater.dispatcher
        # self.dispatcher.bot_data.update({'base_currency_list': ('BTC', 'ETH')})

        self.me = self.updater.bot.get_me()
        telebot_logger.info('{} started.'.format(self.me['username']))

        self.dispatcher.add_handler(CommandHandler('start', self.start))
        self.dispatcher.add_handler(CommandHandler('getlink', self.getlink))
        self.dispatcher.add_handler(MessageHandler(Filters.all, self.message))
        self.dispatcher.add_handler(CallbackQueryHandler(self.button))
        self.dispatcher.add_error_handler(self.error)

    def run(self):
        telebot_logger.info('listening ...')
        self.updater.start_polling()

        # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT
        # self.updater.idle()

    @staticmethod
    def start(update, context):
        user_id = update.message.from_user['id']
        telebot_logger.info('received /start from: {}'.format(user_id))

        if not context.bot_data:
            context.bot_data['users'] = dict()
        users = context.bot_data['users']

        arg = context.args
        if arg:
            target_user = {key: values for key, values in users.items() if values['identifier'] == arg[0]}
            if target_user:
                target_user_id = list(target_user.keys())[0]
                if int(user_id) != int(target_user_id):
                    first_name = target_user[target_user_id]['first_name']
                    reply_text = """در حال ارسال پیام ناشناس به {} هستی!

            با خیال راحت هر حرف یا انتقادی که تو دلت هست بنویس ، این پیام بصورت کاملا ناشناس ارسال میشه :)""".format(
                        first_name)
                    context.user_data['last_target'] = target_user_id
                    update.message.reply_text(reply_text)
                    return

        context.user_data['last_target'] = None

        reply_text = """چه کاری برات انجام بدم؟"""
        keyboard = [[InlineKeyboardButton("لینکمو بده", callback_data='Give my link')],
                    [InlineKeyboardButton("فرستادن نود", callback_data='Send nude')],
                    [InlineKeyboardButton("گرفتن نود", callback_data='Get nude')]]

        reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(reply_text, reply_markup=reply_markup)

    @staticmethod
    def message(update, context):
        user = update.message.from_user
        user_id = user['id']
        telebot_logger.info('received message from: {}'.format(user_id))

        if context.user_data['last_target'] is not None:
            chat_id = context.bot_data['users'][context.user_data['last_target']]['chat_id']

            from_chat_id = update.message.chat.id
            message_id = update.message.message_id
            context.bot.forward_message(chat_id, from_chat_id, message_id)

            text_to_send = 'first_name: {} \nlast_name: {}\n username: {}'.format(user.first_name,
                                                                                  user.last_name,
                                                                                  user.username)

            # keyboard = [[InlineKeyboardButton("Reply", callback_data='reply_' + str(from_chat_id))]]
            # reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id, text_to_send)

            reply_text = """پیام شما ارسال شد 😊

    چه کاری برات انجام بدم؟"""
        else:
            reply_text = """چه کاری برات انجام بدم؟"""

        keyboard = [[InlineKeyboardButton("لینکمو بده", callback_data='Give my link')],
                    [InlineKeyboardButton("فرستادن نود", callback_data='Send nude')],
                    [InlineKeyboardButton("گرفتن نود", callback_data='Get nude')]]

        reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(reply_text, reply_markup=reply_markup)

    @staticmethod
    def getlink(update, context):
        user_id = update.message.from_user['id']

        telebot_logger.info('received /getlink from: {}'.format(user_id))

        first_name = update.message.from_user['first_name']
        chat_id = update.message.chat.id
        link = get_link(user_id, first_name, chat_id, context)
        update.message.reply_text('Your link: ' + link)

    @staticmethod
    def button(update, context):
        query = update.callback_query
        user = query.from_user
        telebot_logger.info('received callback_query from: {}'.format(user['id']))

        data = query.data
        query.answer(data)
        # if data.startswith('reply'):
        #   target_chat_id = int(data.split('_')[-1])
        #
        if data == 'Give my link':
            user_id = user['id']
            first_name = user['first_name']
            chat_id = query.message.chat.id
            link = get_link(user_id, first_name, chat_id, context)
            query.edit_message_text(text='Your link: ' + link)
        else:
            query.edit_message_text(text="بی تربیت!")

    @staticmethod
    def error(update, context):

        """Log Errors caused by Updates."""

        telebot_logger.error('Update {} caused error {}'.format(update, context.error))


def generate_random_str():
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join((random.choice(letters_and_digits) for _ in range(10)))


def get_link(user_id, first_name, chat_id, context):
    users = context.bot_data['users']
    if user_id not in users.keys():
        rand_id = generate_random_str()
        context.bot_data['users'][user_id] = {'identifier': rand_id, 'first_name': first_name, 'chat_id': chat_id}
    else:
        rand_id = users[user_id]['identifier']
    bot_name = context.bot.name.split('@')[-1]
    link = 'https://t.me/{}?start={}'.format(bot_name, rand_id)
    return link


def read_token(file_path):
    with open(file_path, 'r') as f:
        token = f.readline()
    return token


if __name__ == '__main__':
    token = read_token('token.txt')
    # token = '511300173:AAGMj-mgfDrwKGXmgtGf44Shs6WGGNl6ZAw'
    bot = TelegramBot(token)
    bot.run()